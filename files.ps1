#Process which CPU is over 5
Get-Process | ? {$_.CPU -gt 5}

#Services which begin by S and are running
Get-Service S* | ? {$_.Status -like "running"}

#Write both commands in Recap.txt
Get-Process | ? {$_.CPU -gt 5} > Recap.txt
Get-Service | ? {$_.Status -like "running"} >> Recap.txt

#Stop powershell processes (carefull)
Get-Process powershell | Stop-Process

#Create repositories linked to bitlocker enabling
get-command enable*bitlocker* | ? {mkdir $_}

#Repositories created this year (2 versions)
Get-ChildItem C:\ | ? {$_.CreationTime -ge "01/01/2017"}
Get-ChildItem C:\ | ? {$_.CreationTime -gt "12/31/2016"}

#Create 10, 20 and 60 Mb files 
fsutil file createnew C:\labo\fichier10Mb 10000000
fsutil file createnew C:\labo\fichier20Mb 20000000
fsutil file createnew C:\labo\fichier60Mb 60000000

#Classify files over 10 Mb and 50 Mb in Moyen repository
mkdir Moyen
ls -file | ? {$_.Length -gt 10000000 -and $_.Length -lt 50000000} | ? {Move-Item $_.Name moyen}

#Yellow "Powershell rocks" message
Write-host "Powershell rocks" -foregroundcolor Yellow

#4 last commands history in Last4Commands.txt file
get-history -count 4 > Last4Commands.txt 
H -c 4 > Last4Commands.txt 